var manager = 'dimerduo@gmail.com';

function parseUrlQuery() {
    var data = {},
		pair = false,
		param = false;
    if(location.search) {
        pair = (location.search.substr(1)).split('&');
        for(var i = 0; i < pair.length; i ++) {
            param = pair[i].split('=');
            data[param[0]] = param[1];
        }
    }
    return data;
}

function get_pay(deliveryId){
$.get('https://netology-fbb-store-api.herokuapp.com/order/delivery/'+deliveryId+'/payment', function (data){
		var all = data;
		$('.payments').html('');
		$(all).each(function(){
			var name = this.title;
			var id = this.id;
			var input = '<div><input name="paymentId" type="radio" value="'+id+'" ><label>'+name+'</label></div>';
			$('.payments').prepend(input);
		});
	});
}

var id = parseUrlQuery().id;
$('#bookId').val(id);

$.get('https://netology-fbb-store-api.herokuapp.com/book/'+id, function (data){
	var image = data.cover.large;
	var title = data.title;
	var price = data.price;
	var id = data.id;
	var book = '<img data-price="'+price+'" id="'+id+'" class="Book" src="'+image+'">';
	var link = 'single.html?id='+id;
	$('.bookLink').attr('href',link).html(title);
	$('.book').prepend(book);
});

$.get('https://netology-fbb-store-api.herokuapp.com/order/delivery', function (data){
	console.log(data);
	var all = data;
	$(all).each(function(){
		var name = this.name;
		var id = this.id;
		var adress = this.needAdress;
		var price = this.price;
		var input = '<div><input name="deliveryId" type="radio" value="'+id+'" data-adress="'+adress+'" data-price="'+price+'"><label>'+name+'</label></div>';
		$('.delivery').prepend(input);
	});
	$('.delivery input').first().prop( "checked", true );
	var deliveryId = $('.delivery input').first().val();
	get_pay(deliveryId);
	var deliveryCost = $('.delivery input').first().data('price');
	var bookPrice = $('.Book').data('price');
	var total = Number(deliveryCost) + Number(bookPrice);
	$('.totalPrice').html(total);

});

$(document).on('click','.delivery input',function(){
	var id = $(this).val();
	var adress = $(this).data('adress');
	get_pay(id);
	var dCost = $('input[name=deliveryId]:checked').data('price');
	var bookPrice = $('.Book').data('price');	
	var total = Number(dCost) + Number(bookPrice);
	$('.totalPrice').html(total);
	if (adress === true){
		$('#delAdd').show();
	} else {
		$('#delAdd').hide();
	}
	
});

$(document).on('click','.buy',function(){
	$('.vld').removeClass('err');
	$('.errText').html('');
	if ($('.vld').val() === ""){
		$('.vld').addClass('err');
		$('.errText').html('Заполните все поля');
		return false;
	}
	var send = $('form').serializeArray();
	$.ajax({
          type: 'POST',
          url: 'https://netology-fbb-store-api.herokuapp.com/order',
          data: send,
          success: function(data) {
           $('.result').html('<div class="row title"><h2>Заказ на книгу успешно оформлен.<br>спасибо что спасли книгу от сжигания в печи.</h2></div>')
          },
          error:  function(data){
	    $('.err').html(data.responseJSON.message);
          }
        });
});

$.get('https://netology-fbb-store-api.herokuapp.com/currency/', function (data){
	$(data).each(function (item){
		$('<option></option>').html(this.CharCode+' | '+this.Name).attr('data-nominal',this.Nominal).attr('data-value',this.Value).val(this.ID).appendTo('select');
	});
});