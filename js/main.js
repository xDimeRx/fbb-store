function showSmall(obj,target){
	var divClass = "col-md-3 singleBook";
	var image = obj.cover.small;
	var desc = obj.info;
	var link = obj.id;
	var html = '<div class="'+divClass+'"><a href="single.html?id='+link+'"><img src="'+image+'"></a><div class="title"><a href="single.html?id='+link+'">'+desc+'<a></div></div>';
	$(target).prepend(html);
}

var i = 0;
$.get('https://netology-fbb-store-api.herokuapp.com/book', function (data){
	console.log(data);
	data.forEach(function(elem){
		showSmall(elem,'.books');
	});
	var row_count = 4;
	var $e = $('.books');
	while ($e.children('.col-md-3').not('.row').length) {
		$e.children('.col-md-3').not('.row').filter(':lt(' + row_count + ')').wrapAll('<div class="row booksRow">');
	}
	$('.booksRow:gt('+i+')').hide();
	
});

$('.more').click(function(){
	$('.booksRow:hidden').first().show();
	if ( $('.booksRow:hidden').length === 0 ){
		$('.btn.more').hide();
	}
	i++;
});