function parseUrlQuery() {
    var data = {},
	pair = false,
	param = false;
    if(location.search) {
        pair = (location.search.substr(1)).split('&');
        for(var i = 0; i < pair.length; i ++) {
            param = pair[i].split('=');
            data[param[0]] = param[1];
        }
    }
    return data;
}

var id = parseUrlQuery().id;

$('.buy').attr('href','order.html?id='+id);

var book = $.get('https://netology-fbb-store-api.herokuapp.com/book/'+id, function (data){
	console.log(data);
	var divClass = "book";
	var image = data.cover.large;
	var desc = data.description;
	var price = data.price;
	var book = '<div class="'+divClass+'"><img src="'+image+'"><div class="sb_title">'+desc+'</div></div>';
	$('.book').prepend(book);
	
	var review = data.reviews;
	review.forEach(function(elem){
		var authorImg = elem.author.pic;
		var cite = elem.cite;
		var rewiew = '<div class="col-md-12 review"><img src="'+authorImg+'"><div class="rewTex">'+cite+'</div></div>';
		$('.reviews').prepend(rewiew);
	});
	
	var features = data.features;
	features.forEach(function (elem){
		var pic = elem.pic;
		var text = elem.title;
		var feature = '<div class="col-md-12 feature"><img src="'+pic+'"><div class="fcrTex">'+text+'</div></div>';
		$('.features').prepend(feature);
	});
	
	price = data.price;
	$('.buy').html('Купить за жалкие '+price+' <s>Z</s>');
});

$(document).ready(function(){
	$(document).mousemove(function(e){
		var intence = 4;
		var radius = 100/2;
		var sizeX = $(".eyeCenter").width();
		var sizeY = $(".eyeCenter").height();
		var pos = $('.eye').offset();
		var height = $('.eye').height();
		var width = $('.eye').width();
		var center = {};
		center.y = pos.top + (width/2);
		center.x = pos.left + (height/2);
		var eye = {};
		eye.l = Math.sqrt(Math.pow((center.y - e.pageY),2) + Math.pow((center.x - e.pageX),2)) / intence;
		eye.rot = Math.atan((center.y - e.pageY) / (center.x - e.pageX)) / Math.PI * 180 + 180;
		if ((center.x - e.pageX) < 0) {
		  eye.rot -= 180;
		}
		if (eye.rot < 0)
		  eye.rot += 360;
		if (eye.l > radius) {
			eye.l = radius;
		}
		eye.x = ((eye.l) * Math.cos((eye.rot * Math.PI) / 180) + center.x) - sizeX/2; 
		eye.y = ((eye.l) * Math.sin((eye.rot * Math.PI) / 180) + center.y) - sizeY/2;
		$(".eyeCenter").offset({top:eye.y, left:eye.x});
	});
});